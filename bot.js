const{ ActivityHandler} = require('botbuilder')


class BotActivityHandler extends ActivityHandler{
    constructor(conversationState, userState,dialog){
        super();

        if(!conversationState) throw new Error("Con State Required");
        if (!userState) throw new Error('[DialogBot]: Missing parameter. userState is required');
        if (!dialog) throw new Error('[DialogBot]: Missing parameter. dialog is required');
        


        this.conversationState = conversationState;
        this.userState = userState;
        this.dialog = dialog;        
        this.accessor= this.conversationState.createProperty('DialogAccessor');
        

        this.onMessage(async(context, next)=>{
        
            await this.dialog.run(context, this.accessor);
            await next();
        });

        this.onConversationUpdate( async(context, next)=>{
            //console.log(context.activity);
            if(context.activity.membersAdded && context.activity.membersAdded[1].id == context.activity.from.id){
                await context.sendActivity(" Hello User, Welcome! Say Hi.");     
            }
            await next();
        })
    }
    async run(context){
        await super.run(context);
        await this.conversationState.saveChanges(context,false);
    }
    
}

module.exports.BotActivityHandler = BotActivityHandler;