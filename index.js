const restify = require('restify');
const{BotFrameworkAdapter, ConversationState, MemoryStorage, UserState }= require('botbuilder');
const { BotActivityHandler } = require('./bot');
const { UserProfileDialog } = require('./Dialogs/userProfileDialog');

//adapter

const adapter = new BotFrameworkAdapter({
    appID :' ',
    appPassword : ' '

});

adapter.onTurnError = async(context,error) => {
    console.log("error occured", error);
    await context.sendActivity('Bot encountered an error');

};

const memory = new MemoryStorage();

// Create conversation state with in-memory storage provider.
const conversationState = new ConversationState(memory);
const userState = new UserState(memory);

const dialog = new UserProfileDialog(userState);

const mainBot = new BotActivityHandler(conversationState, userState,dialog);


let server = restify.createServer();
server.listen(3978, ()=>{
        console.log(`${server.name} listening to ${server.url}`);
    });
    
server.post('/api/messages', (req, res) => {
        adapter.processActivity(req, res, async (context) => {
            await mainBot.run(context);
        });
    });    
